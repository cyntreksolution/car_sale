<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleAccessories extends Model
{
    use SoftDeletes;
     protected $fillable = [
        'custom_spec',
        'vehicle_accessoriescol',
        'vehicles_id'
            
       
    ];
}
