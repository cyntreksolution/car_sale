<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarModel extends Model
{
    use SoftDeletes;
    protected $table ='models';
    protected $fillable = [
        'name',
        'description',

    ];
}
