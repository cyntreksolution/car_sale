<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleModel extends Model
{
    use SoftDeletes;
    protected $table = 'models';
    protected $fillable = [
    	'brand_id',
        'name',
        'description',
       
    ];
     public function brand()
    {
        return $this->belongsTo(Brand::class)->withTrashed();
    }
}
