<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     protected $fillable = [
     	'vehicle_id',
        'name',
        'email',
        'telephone',
        'comment'

    ];
}
