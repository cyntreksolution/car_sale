<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class VehicleSpecifications extends Model
{
    use SoftDeletes;
     protected $fillable = [
        'mileage',
        'transmission',
        'year',
        'fuel_type',
        'engine_capacity',
        'color',
        'shape',
        'doors',
        'seats',
        'vehicle_id'


    ];
}
