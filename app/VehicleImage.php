<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VehicleImage extends Model
{
     protected $fillable = ['vehicles_id', 'file_name'];

    public function vehicle()
    {
        return $this->belongsTo('App\Vehicle');
    }
}
