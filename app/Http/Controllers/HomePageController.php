<?php

namespace App\Http\Controllers;

use App\Brand;
use App\CarModel;
use App\Contact;
use App\Mail\ContactUs;
use App\Mail\OrderNow;
use App\Type;
use App\Vehicle;
use App\Order;
use App\VehicleModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomePageController extends Controller
{
    public function index(Request $request)
    {
        $types = Type::all();
        $deals = Vehicle::all();
        $bestSales = Vehicle::all();
        $brands = Brand::all();
        $models = CarModel::all();
        if($request->brand){
            $models = CarModel::whereBrandId($request->brand)->get();
        }

        return view('welcome', compact('types', 'deals', 'models', 'brands', 'bestSales'));
    }

    public function listing(Request $request)
    {
        $types = Type::all();
        $brands = Brand::all();
        $models = CarModel::all();
        $vehicles = Vehicle::query();
        if($request->brand){
            $models = CarModel::whereBrandId($request->brand)->get();
        }

        if (!empty($request->brand)) {
            $vehicles = $vehicles->whereBrandId($request->brand);
        }

        if (!empty($request->model)) {
            $vehicles = $vehicles->whereModelId($request->model);
        }

        if (!empty($request->year)) {
            $vehicles = $vehicles->whereHas('specification', function ($q) use ($request) {
                return $q->where('year','='.$request->year);
            });
        }

        $vehicles = $vehicles->get();


        return view('listing', compact('types', 'models', 'brands', 'vehicles'));
    }

    public function aboutus()
    {
        return view('aboutus');
    }

    public function contactus()
    {
        return view('contactus');
    }

    public function contactusstore(Request $request)
    {

        $contact = Contact::create($request->all());
        Mail::to('info@as-exports.co.uk')->cc($contact->email)->send(new ContactUs($contact));
        return redirect(route('contactus'));

    }

    public function order_now()
    {
        $brands = Brand::pluck('name', 'id');
        $models = VehicleModel::pluck('name', 'id');
        $types = Type::pluck('name', 'id');
        $vehicles = Vehicle::all();
        return view('order-now', compact('brands', 'models', 'types', 'vehicles'));
    }

    public function ordernowstore(Request $request)
    {
        $order = Order::create($request->all());
//        return (new OrderNow($order))->render();
        Mail::to('info@as-exports.co.uk')->cc($order->email)->send(new OrderNow($order));
        return redirect(route('order-now'));

    }


}
