<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brand;
use App\VehicleModel;
use App\Type;
use App\Vehicle;
use App\VehicleImage;
use App\VehicleSpecifications;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicles = Vehicle::all();
        return view('admin.vehicle.index', compact('vehicles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::pluck('name', 'id');
        $models = VehicleModel::pluck('name', 'id');
        $types = Type::pluck('name', 'id');
        return view('admin.vehicle.create', compact('brands', 'models', 'types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $validatedData = $request->validate([
        //     'brand_id' => 'required',
        //     'model_id' => 'required',
        //     'type_id' => 'required',
        //     'title' => 'required',
        //     'overview' => 'required',
        // ]);


        $vehicle = Vehicle::create([
            'brand_id' => $request->brand,
            'model_id' => $request->model,
            'type_id' => $request->type,
            'title' => $request->title,
            'price' => $request->price,
            'start_price' => $request->start_price,
            'end_price' => $request->end_price,
            'overview' => $request->overview,
            'additional_spec' => $request->additional_spec,
            'featured' => !empty($request->featured)?1:0
        ]);


        VehicleSpecifications::create([
            'vehicle_id' => $vehicle->id,
            'mileage' => $request->mileage,
            'transmission' => $request->transmission,
            'year' => $request->year,
            'fuel_type' => $request->fuel_type,
            'engine_capacity' => $request->engine_capacity,
            'color' => $request->color,
            'shape' => $request->shape,
            'doors' => $request->doors,
            'seats' => $request->seats

        ]);

        if (!empty($request->photos)){
            foreach ($request->photos as $photo) {
                $destinationPath = 'uploads/vehicles/';
                $filename = $photo->getClientOriginalName();
                $photo->move($destinationPath, $filename);
                $vehicleImage = new VehicleImage();
                $vehicleImage->file_name = $filename;
                $vehicleImage->vehicle_id = $vehicle->id;
                $vehicleImage->save();
            }
        }

        return  redirect(route('vehicles.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Vehicle $vehicle)
    {

        return view('admin.vehicle.show', compact('vehicle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicle = Vehicle::with('specification')->findOrFail($id);
        $brands = Brand::pluck('name', 'id');
        $models = VehicleModel::pluck('name', 'id');
        $types = Type::pluck('name', 'id');
        return view('admin.vehicle.edit', compact('brands', 'models', 'types', 'vehicle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'brand_id' => 'required',
            'model_id' => 'required',
            'type_id' => 'required',
            'title' => 'required',
            'overview' => 'required',
        ]);


        $vehicle = Vehicle::with('specification')->find($id);
        $vehicle->brand_id = $request->brand_id;
        $vehicle->model_id = $request->model_id;
        $vehicle->type_id = $request->type_id;
        $vehicle->title = $request->title;
        $vehicle->price = $request->price;
        $vehicle->start_price = $request->start_price;
        $vehicle->end_price = $request->end_price;
        $vehicle->overview = $request->overview;
        $vehicle->featured = !empty($request->featured)?1:0;
        
                if (!empty($request->photos)){
            foreach ($request->photos as $photo) {
                $destinationPath = 'uploads/vehicles/';
                $filename = $photo->getClientOriginalName();
                $photo->move($destinationPath, $filename);
                $vehicleImage = new VehicleImage();
                $vehicleImage->file_name = $filename;
                $vehicleImage->vehicle_id = $vehicle->id;
                $vehicleImage->save();
            }
        }
        
        $vehicle->save();
        $request->merge(['airbags' => $request->input('airbags', 0)]);
        $vehicle->specification->update([
            'mileage'=>$request->mileage, 'transmission'=>$request->transmission, 'year'=>$request->year, 'fuel_type'=>$request->fuel_type, 'engine_capacity'=>$request->engine_capacity, 'color'=>$request->color,
            'shape'=>$request->shape, 'doors'=>$request->doors, 'seats'=>$request->seats]);
        return  redirect(route('vehicles.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $show = Vehicle::findOrFail($id);

        $show->specification()->delete();
        $show->images()->delete();
        $show->delete();
        return 'true';
    }

     public function imagedestroy(Request $request)
    {
        $show = VehicleImage::findOrFail($request->id);
        $show->delete();
        return 'true';
    }
}
