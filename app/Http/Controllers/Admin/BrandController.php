<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Brand;
use File;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $brands = Brand::all();
        return view('admin.brand.index', compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validatedData = $request->validate([
            'name' => 'required',
           
        ]);
         $brand = Brand::create($request->all());
         $file = $request->file('logo');
        if (!empty($file)) {
            $destinationPath = 'uploads/brand/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $brand->logo = $filename;
            $brand->save();
        }
        
    
           return redirect(route('brands.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $brand = Brand::findOrFail($id);

     return view('admin.brand.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $validatedData = $request->validate([
            'name' => 'required',
           
        ]);
        
        
        $brand->name=$request->name;
        $brand->featured=!empty($request->featured)?1:0;
          $file = $request->file('logo');
        if (!empty($file)) {
            $destinationPath = 'uploads/brand/';
            File::delete($destinationPath.$brand->logo);//delete current image from storage
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $brand->logo = $filename;
        }
        $brand->save();

        return redirect(route('brands.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $brand = Brand::findOrFail($id);
        $brand->delete();
         return 'true';
    }
}
