<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use App\CarModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\VehicleModel;


class ModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = VehicleModel::all();
        return view('admin.model.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = Brand::pluck('name', 'id');
        return view('admin.model.create',compact('brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',

        ]);
        $model = new VehicleModel();
        $model->name = $request->name;
        $model->brand_id = $request->brand;
        $model->description = $request->description;
        $model->save();


        return redirect(route('models.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brands = Brand::pluck('name', 'id');
          $model = VehicleModel::findOrFail($id);

     return view('admin.model.edit', compact('model','brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',

        ]);
        
        $model= VehicleModel::find($id);
        $model->name=$request->name;
        $model->brand_id=$request->brand;
        $model->description=$request->description;
        $model->save();

        return redirect(route('models.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $show = VehicleModel::findOrFail($id);
        $show->delete();
        return 'true';
    }

    public function modelOdBrand(Request $request, Brand $brand)
    {
        $models = CarModel::whereBrandId($brand->id)->pluck('name', 'id');
        $txt = '';
        foreach ($models as $key => $model) {
            $txt.= "<option>" . $model . "</option>";
        }

        return $txt;
    }

}
