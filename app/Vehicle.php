<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vehicle extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'brand_id',
        'model_id',
        'type_id',
        'title',
        'price',
        'start_price',
        'end_price',
        'overview',
        'additional_spec',
        'featured'


    ];

    public const years = [
        2020 => 2020,
        2019 => 2019,
        2018 => 2018,
        2017 => 2017,
        2016 => 2016,
        2015 => 2015,
        2014 => 2014,
        2013 => 2013,
        2012 => 2012,
        2011 => 2011,
        2010 => 2010,
        2009 => 2009,
        2008 => 2008,
        2007 => 2007,
        2006 => 2006,
        2005 => 2005,
        2004 => 2004,
        2003 => 2003,
        2002 => 2002,
        2001 => 2001,
        2000 => 2000,
    ];

    public function images()
    {
        return $this->hasMany(VehicleImage::class);
    }

    public function specification()
    {
        return $this->hasOne(VehicleSpecifications::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class)->withTrashed();
    }

    public function model()
    {
        return $this->belongsTo(VehicleModel::class)->withTrashed();
    }

    public function type()
    {
        return $this->belongsTo(Type::class)->withTrashed();
    }
}
