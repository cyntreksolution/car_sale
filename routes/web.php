<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomePageController@index')->name('home');
Route::get('aboutus', 'HomePageController@aboutus')->name('aboutus');
Route::get('contactus', 'HomePageController@contactus')->name('contactus');
Route::post('contactus', 'HomePageController@contactusstore')->name('contactus');
Route::get('order-now', 'HomePageController@order_now')->name('order-now');
Route::post('order-now', 'HomePageController@ordernowstore')->name('order-now');
Route::post('filter', 'HomePageController@filter')->name('vehicle.filter');
Route::get('listing', 'HomePageController@listing')->name('listing.index');
Route::get('/car/{vehicle}', 'Admin\DashboardController@car')->name('single.car');
Route::get('/brand/{brand}/model', 'Admin\ModelController@modelOdBrand')->name('brand.model');

Auth::routes();
if (!env('ALLOW_REGISTRATION', false)) {
    Route::any('/register', function () {
        abort(403);
    });
}
Route::middleware(['auth'])->group(function () {
    Route::get('/console', 'Admin\DashboardController@index');
//    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('types', 'Admin\TypeController');

    Route::resource('brands', 'Admin\BrandController');

    Route::resource('models', 'Admin\ModelController');

    Route::resource('vehicles', 'Admin\VehicleController');

    Route::post('vehicles/imagedestroy', 'Admin\VehicleController@imagedestroy');

     Route::resource('users', 'Admin\UserController');

     Route::post('/users/update-pass/{id}','Admin\UserController@updatepass')->name('users.updatepass');

    Route::get('/dashboard', 'Admin\VehicleController@index')->name('dashboard');

});


