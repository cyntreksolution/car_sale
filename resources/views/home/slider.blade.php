<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="section-filter">
                <div class="b-filter__inner bg-grey">
                    <h2>Find your right car</h2>
                        {!!  Form::open(['route' => 'listing.index','class'=>'b-filter','method'=>'get']) !!}
                        <div class="btn-group bootstrap-select" style="width: 100%;">
                            {!! Form::select('brand',$brands->pluck('name','id'),null,['class'=>'selectpicker','data-width'=>"100%",'placeholder'=>'Select Brand','id'=>'brand_drop']) !!}
                        </div>

                       <!--  <div class="btn-group bootstrap-select" style="width: 100%;">
                            {!! Form::select('model',$models->pluck('name','id'),null,['class'=>'selectpicker','data-width'=>"100%",'placeholder'=>'Select Model','id'=>'model_drop']) !!}
                        </div> -->

{{--                        <div class="btn-group bootstrap-select" style="width: 100%;">--}}
{{--                            {!! Form::select('year',\App\Vehicle::years,null,['class'=>'selectpicker','data-width'=>"100%",'placeholder'=>'Select Year']) !!}--}}
{{--                        </div>--}}
{{--                        <div class="ui-filter-slider">--}}
{{--                            <div class="sidebar-widget-body m-t-10">--}}
{{--                                <div class="price-range-holder"> <span class="min-max"> <span class="pull-left">$200.00</span> <span class="pull-right">$800.00</span> </span>--}}
{{--                                    <input type="text" class="price-slider" value="" style="display:block" >--}}
{{--                                </div>--}}
{{--                                <!-- /.price-range-holder -->--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div>
                            <div class="b-filter__btns">
                                <button type="submit" class="btn btn-lg btn-primary">search vehicle</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <!-- Slider -->
        <div class="home-slider5">
            <div id="thmg-slideshow" class="thmg-slideshow">
                <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container' >
                    <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
                        <ul>
                            <li data-transition='fade' data-slotamount='7' data-masterspeed='1000' >
                                <img src='{{asset('images/slider/lambo.jpg')}}'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/>
                                <div class="container">
                                    <div class="content_slideshow">
                                        <div class="row">
                                            <div class="col-lg-3 col-sm-3 col-md-3 "> &nbsp;</div>
{{--                                                <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3; white-space:nowrap;'><span style="font-weight:normal; display:block">#1</span> Trustability and Quality is a Gift from us to you </div></div>--}}
                                            <div class="col-lg-9 col-sm-9 col-md-9">
                                                <div class="info">
{{--                                                    <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap;'><span>Trustability</span> </div>--}}
                                                    <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;' ><span style="font-weight:normal; display:block">Trustability and Quality is a Gift  <br> from us to you</span> </div>
{{--                                                    <div class='tp-caption Title sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1450' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'>Get 40% OFF on selected items.</div>--}}
{{--                                                    <div class='tp-caption sfb  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'><a href='#' class="buy-btn">Book Appointment</a> </div>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li data-transition='fade' data-slotamount='7' data-masterspeed='1000' >
                                <img src='{{asset('images/slider/bmws.jpg')}}'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/>
                                <div class="container">
                                    <div class="content_slideshow">
                                        <div class="row">
                                            <div class="col-lg-3 col-sm-3 col-md-3">&nbsp;</div>
                                            <div class="col-lg-9 col-sm-9 col-md-9">
                                                <div class="info">
{{--                                                    <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap;'><span>Top Brands 2018</span> </div>--}}
                                                    <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;' ><span style="font-weight:normal; display:block">Trustability and Quality is a Gift  <br> from us to you</span> </div>
{{--                                                    <div class='tp-caption Title sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1450' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'>Get 40% OFF on selected items.</div>--}}
{{--                                                    <div class='tp-caption sfb  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'><a href='#' class="buy-btn">Book Appointment</a> </div>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li data-transition='fade' data-slotamount='7' data-masterspeed='1000' ><img src='{{asset('images/slider/mercedes_amg_gt.jpg')}}'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/>
                                <div class="container">
                                    <div class="content_slideshow">
                                        <div class="row">
                                            <div class="col-lg-3 col-sm-3 col-md-3">&nbsp;</div>
                                            <div class="col-lg-9 col-sm-9 col-md-9">
                                                <div class="info">
{{--                                                    <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap;'><span>Top Brands 2018</span> </div>--}}
                                                    <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;' ><span style="font-weight:normal; display:block ;color: black !important;">Trustability and Quality is a Gift  <br> from us to you</span> </div>
{{--                                                    <div class='tp-caption Title sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1450' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'>Get 40% OFF on selected items.</div>--}}
{{--                                                    <div class='tp-caption sfb  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'><a href='#' class="buy-btn">Book Appointment</a> </div>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li data-transition='fade' data-slotamount='7' data-masterspeed='1000' ><img src='{{asset('images/slider/range_rover_velar_121.jpg')}}'  data-bgfit='cover' data-bgrepeat='no-repeat' alt="banner"/>
                                <div class="container">
                                    <div class="content_slideshow">
                                        <div class="row">
                                            <div class="col-lg-3 col-sm-3 col-md-3">&nbsp;</div>
                                            <div class="col-lg-9 col-sm-9 col-md-9">
                                                <div class="info">
{{--                                                    <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2; white-space:nowrap;'><span>Top Brands 2018</span> </div>--}}
                                                    <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;' ><span style="font-weight:normal; display:block">Trustability and Quality is a Gift  <br> from us to you</span> </div>
{{--                                                    <div class='tp-caption Title sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1450' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'>Get 40% OFF on selected items.</div>--}}
{{--                                                    <div class='tp-caption sfb  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4; white-space:nowrap;'><a href='#' class="buy-btn">Book Appointment</a> </div>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div class="tp-bannertimer"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

