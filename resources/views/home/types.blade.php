<div class="top-cate">
    <div class="featured-pro container">
        <div>
            <div class="slider-items-products">
                <div class="new_title">
                    <h2>browse by car type </h2>
                </div>
                <div id="top-categories" class="product-flexslider hidden-buttons">
                    <div class="slider-items slider-width-col4 products-grid">

                        @foreach($types as $type)
                        <div class="item">
                            <div class="pro-img"><img src="images/car-type1.png" alt="Retis lapen casen">
                                <div class="pro-info">{{$type->name}}</div>
                            </div>
                        </div>
                        @endforeach

{{--                        <!-- Item -->--}}
{{--                        <div class="item">--}}
{{--                            <div class="pro-img"><img src="images/car-type2.png" alt="Retis lapen casen">--}}
{{--                                <div class="pro-info">Trucks</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- End Item -->--}}
{{--                        <!-- Item -->--}}
{{--                        <div class="item">--}}
{{--                            <div class="pro-img"><img src="images/car-type7.png" alt="Retis lapen casen">--}}
{{--                                <div class="pro-info">SUVS‎</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- End Item -->--}}
{{--                        <!-- Item -->--}}
{{--                        <div class="item">--}}
{{--                            <div class="pro-img"><img src="images/car-type3.png" alt="Retis lapen casen">--}}
{{--                                <div class="pro-info">Convertibles</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- End Item -->--}}

{{--                        <!-- Item -->--}}
{{--                        <div class="item">--}}
{{--                            <div class="pro-img"><img src="images/car-type5.png" alt="Retis lapen casen">--}}
{{--                                <div class="pro-info">Hybrids</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- End Item -->--}}
{{--                        <!-- Item -->--}}
{{--                        <div class="item">--}}
{{--                            <div class="pro-img"><img src="images/car-type9.png" alt="Retis lapen casen">--}}
{{--                                <div class="pro-info">Hatchbacks</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- End Item -->--}}
{{--                        <!-- Item -->--}}
{{--                        <div class="item">--}}
{{--                            <div class="pro-img"><img src="images/car-type6.png" alt="Retis lapen casen">--}}
{{--                                <div class="pro-info">Coupes</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- End Item -->--}}

{{--                        <!-- Item -->--}}
{{--                        <div class="item">--}}
{{--                            <div class="pro-img"><img src="images/car-type8.png" alt="Retis lapen casen">--}}
{{--                                <div class="pro-info">Wagons</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- End Item -->--}}

{{--                        <!-- Item -->--}}
{{--                        <div class="item">--}}
{{--                            <div class="pro-img"><img src="images/car-type10.png" alt="Retis lapen casen">--}}
{{--                                <div class="pro-info">Luxury</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- End Item -->--}}

{{--                        <!-- Item -->--}}
{{--                        <div class="item">--}}
{{--                            <div class="pro-img"><img src="images/car-type12.png" alt="Retis lapen casen">--}}
{{--                                <div class="pro-info">Pickup</div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <!-- End Item -->--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
