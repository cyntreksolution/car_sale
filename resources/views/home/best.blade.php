<section class=" wow bounceInUp animated">
    <div class="best-pro slider-items-products container">
        <div class="new_title">
            <h2>Best Sellers</h2>
        </div>

        <div id="best-seller" class="product-flexslider hidden-buttons">
            <div class="slider-items slider-width-col4 products-grid">
                @foreach($bestSales as $deal)
                    <div class="item">
                        <div class="item-inner">
                            <div class="item-img">
                                <div class="item-img-info">
                                    <a href="{{route('single.car',[$deal->id])}}" title="Retis lapen casen"
                                       class="product-image">
                                        <img style="max-height: 165px" src="{{asset('uploads/vehicles/'.$deal->images[0]->file_name)}}"
                                             alt="Retis lapen casen"></a>
                                </div>
                            </div>
                            <div class="item-info">
                                <div class="info-inner">
                                    <div class="item-title">
                                        <a href="{{route('single.car',[$deal->id])}}"
                                           title="Retis lapen casen">{{$deal->title}}</a>
                                    </div>
                                    <div class="item-content">
                                        <div class="rating">
                                            {{--                                            <div class="ratings">--}}
                                            {{--                                                <div class="rating-box">--}}
                                            {{--                                                    <div class="rating" style="width:80%"></div>--}}
                                            {{--                                                </div>--}}
                                            {{--                                                <p class="rating-links"><a href="#">1 Review(s)</a> <span--}}
                                            {{--                                                        class="separator">|</span> <a href="#">Add Review</a></p>--}}
                                            {{--                                            </div>--}}
                                        </div>
                                        <div class="item-price">
                                            <div class="price-box">
                                                <span class="regular-price"><span
                                                        class="price">{{($deal->price)?$deal->price:'Negotiate'}}</span> </span>
                                            </div>
                                        </div>
                                        <div class="other-info">
                                            <div class="col-km"><i
                                                    class="fa fa-tachometer"></i>{{optional($deal->specification)->mileage}}
                                                km
                                            </div>
                                            <div class="col-engine"><i
                                                    class="fa fa-gear"></i> {{optional($deal->specification)->transmission}}
                                            </div>
                                            <div class="col-date"><i class="fa fa-calendar"
                                                                     aria-hidden="true"></i> {{optional($deal->specification)->year}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
