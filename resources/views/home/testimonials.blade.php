<div class="brand-logo wow bounceInUp animated">
    <div class="container">
        <div class="row">
            <div class="home-banner col-lg-2 hidden-md col-xs-12 hidden-sm"></div>
            <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <div class="testimonials-section">
                    <div class="offer-slider parallax parallax-2">
                        <ul class="bxslider">
                            <li>
                                <div class="avatar"><img src="images/erosh.jpg" alt="Image"></div>
                                <div class="testimonials"> Working with A & S has been a terrific experience. Mr. Roshan has specialised in this industry for several years, and as a company we have personally been greatly satisfied with our service. They offer a wide range of professional knowledge and provide great customer service. This gives a strong sense of trust when dealing with luxury vehicles. Highly recommend such a fantastic company.
                                </div>
                                <div class="clients_author">Erosha Traders (PVT) LTD <span>Erosha Gamage</span></div>
                            </li>
                            <li>
                                <div class="avatar"><img src="images/slider/testimonials.jpeg" alt="Image"></div>
                                <div class="testimonials">I am very glad to have a supplier like you who always committed to   provide the best service. You are the reason for my success in the European vehicles sector.
                                </div>
                                <div class="clients_author">Gayuki Holdings <span>Dushantha Kumara</span></div>
                            </li>
                            <li>
                                <div class="avatar"><img src="images/crone.jpg" alt="Image"></div>
                                <div class="testimonials">Having being working over 5 years as a service provider to A & S UK, We have no hesitation to recommend as the best company among our customers.
                                </div>
                                <div class="clients_author">Clarion Logistics Pvt Ltd <span>Mohan Meehitiya</span></div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
