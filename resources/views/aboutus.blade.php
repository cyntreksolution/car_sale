@extends('layouts.master')

@section('content')
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title">
                        <h2>About Us</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN Main Container -->

    <div class="main-container col1-layout wow bounceInUp animated animated" style="visibility: visible;">

        <div class="main container blog_entry">
            <div class="row">
                <div class="std">
                    <div class="wrapper_bl" style="margin-top: 1px;">

                        <div class="form_background">
                            <div class="row">
                                <div class="col-md-6 thm-post ">
                                    <div class="blog_entry-header-inner">
                                        <h1 class="blog_entry-title"> We are A and S Imports and Exports LTD</h1>
                                    </div>

                                    <p>we give you a complete export sale service for a vast range of top quality cars
                                        and
                                        commercial vehicles.</p>

                                    <p>
                                        We are A and S Imports and Exports LTD, we give you a complete export sale service for a vast range of top quality cars and commercial vehicles.
You can trust our experience. We have been shipping vehicles from the UK all over the world since 2004, and have worked for many satisfied customers. You will find Mercedes Benz, Toyota and Land Rover as well as other quality manufacturers on our web site. If we don’t have the vehicle that you are looking for in our stock, we can find a vehicle of your choice. We, A and S Imports and Exports, only sell the best quality new or used cars and commercial vehicles. Furthermore, we are a UK company, based in Leicester in the centre of the UK, so you can be sure that we are here to oversee and control every step of the process.
You can rely on us because every single vehicle we export comes with the following:<br><br>
 <ul>
<li>A full mechanical and bodywork inspection by our engineers</li>
<li>A full twelve month “MOT” certificate issued by an independent testing station authorised by the UK Government. This certificate shows that the vehicle has passed the full test required to be roadworthy in the European Union.</li><br>
<li>Lastly,a six month guarantee from us that the vehicle is in first class mechanical conditions</li></ul><br>
You get a complete service with us. A and S Imports and Exports can arrange the shipping so that the vehicle will arrive safely and quickly in your country, with all the paperwork you need to import it. We ship to many places around the globe including South Africa, Malaysia, Sri Lanka, Thailand, Japan, Trinidad, Papua New Guinea and many more. We can also advise you on the papers you will need in your own country.<br>
Contact us now about any of our vehicles, and find out how easy it can be for you to have one of these top quality European cars or vans shipped to you.<br><br>


VAT 20%
If you are outside the European Union then the price you see on our website is the price you pay. You only have to add the costs of shipping to your country. We have highlighted some of the cars as “vat registered”. This means that they were owned by a business in the UK, and registered for the local sales tax called vat. This is currently set at 20%. If you live outside the EU then A and S Imports and Exports reclaim this tax for you, so the price is 20% cheaper than it would be in the UK.  This means that cars are often better value for you. Remember, if you are outside the European Union, the price you see is the price you pay.<br><br>



How we get the vehicle to you:
Firstly we deliver the vehicle to the port using a car carrier to ensure no miles are driven on your car, up until you have received it. Once at the port the vehicles will be loaded into a container of up to 4 cars. It will be up to you whether to have your car in a container on its own or with 1, 2 or 3 other cars. The vehicle will then be loaded of at the port you have requested to have it delivered to; the vehicle is then all yours.

                                    </p>



                                </div>
                                <div class="col-md-6">
                                    <img src="{{asset('images/slider/jam.jpg')}}" class="img img-responsive">
                                </div>
                            </div>
                            <div class="row mt-3" style="margin-top: 20px">
                                <div class="col-md-6">
                                    <img src="{{asset('images/slider/cars.jpg')}}" class="img img-responsive">
                                </div>
                                <div class="col-md-6 thm-post ">

                                    <h3> You can rely on us because every single vehicle we export comes with the
                                        following:
                                    </h3>

                                    <p class="mt-2"> A full mechanical and bodywork inspection by our engineers
                                    A full twelve month “MOT” certificate issued by an independent testing station
                                    authorised by the UK Government. This certificate shows that the vehicle has passed
                                    the full test required to be roadworthy in the European Union.
                                    Lastly,a six month guarantee from us that the vehicle is in first class mechanical
                                    conditions
                                    You get a compete service with us. A and S Imports and Exports can arrange the
                                    shipping so that the vehicle will arrive safely and quickly in your country, with
                                    all the paperwork you need to import it. We ship to many countries including South
                                    Africa, Malaysia, Sri Lanka, Thailand, Japan, Trinidad, Papua New Guinea and many
                                        more. We can also advise you on the papers you will need in your own country.</p>

                                    <p> Contact us now about any of our vehicles, and find out how easy it can be for
                                        you to have one of these top quality European cars or vans shipped to you.</p>

                                </div>

                            </div>
                            <div class="row mt-3" style="margin-top: 20px">
                                <div class="col-md-6 thm-post ">

                                    <h3> You can rely on us because every single vehicle we export comes with the
                                        following:
                                    </h3>

                                    <p class="mt-2"> A full mechanical and bodywork inspection by our engineers
                                    A full twelve month “MOT” certificate issued by an independent testing station
                                    authorised by the UK Government. This certificate shows that the vehicle has passed
                                    the full test required to be roadworthy in the European Union.
                                    Lastly,a six month guarantee from us that the vehicle is in first class mechanical
                                    conditions
                                    You get a compete service with us. A and S Imports and Exports can arrange the
                                    shipping so that the vehicle will arrive safely and quickly in your country, with
                                    all the paperwork you need to import it. We ship to many countries including South
                                    Africa, Malaysia, Sri Lanka, Thailand, Japan, Trinidad, Papua New Guinea and many
                                        more. We can also advise you on the papers you will need in your own country.</p>

                                    <p> Contact us now about any of our vehicles, and find out how easy it can be for
                                        you to have one of these top quality European cars or vans shipped to you.</p>

                                </div>
                                <div class="col-md-6">
                                    <img src="{{asset('images/slider/load.png')}}" class="img img-responsive">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!--main-container-->

    </div> <!--col1-layout-->


@endsection
