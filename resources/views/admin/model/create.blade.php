@extends('admin.layout.master')
@section('content')
<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
                Vehicles Model
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Vehicles Model</a></li>
                <li class="breadcrumb-item active" aria-current="page"> Model</li>
                </ol>
            </nav>
          </div>
            <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Vehicles Model</h4>
                  <form class="cmxform"  method="POST" action="{{ route('models.store') }}">
                      @csrf
                    <fieldset>
                      <div class="form-group row">
                       <div class="col-md-6">
                                            <label for="firstname">Brand</label>
                                            <select class="form-control" required  name="brand">
                                                <option>Select Brand</option>
                                                @foreach ($brands as $key => $value)
                                                    <option value="{{ $key }}">
                                                        {{$value}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                      <div class="col-md-6">
                        <label for="firstname">Name</label>
                        <input id="firstname" class="form-control" name="name" type="text">
                      </div>
                    </div>
                      <div class="form-group">
                        <label for="lastname">Description</label>
                        <textarea id="lastname" rows="5" class="form-control" name="description" type="text"></textarea>
                      </div>
                      
                      <input class="btn btn-primary" type="submit" value="Submit">
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
      
</div>
@endsection