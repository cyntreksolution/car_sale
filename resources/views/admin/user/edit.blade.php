@extends('admin.layout.master')
@section('content')
<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
                Users Details
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Users Details</a></li>
                <li class="breadcrumb-item active" aria-current="page"> Users</li>
                </ol>
            </nav>
          </div>
            <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Users Details</h4>
                  @if (count($errors) > 0)
                    <div class="alert alert-danger">
                      <strong>Whoops!</strong> There were some problems with your input.<br><br>
                      <ul>
                         @foreach ($errors->all() as $error)
                           <li>{{ $error }}</li>
                         @endforeach
                      </ul>
                    </div>
                  @endif
                  <form class="cmxform"  method="POST" action="{{ route('users.updatepass',$user->id) }}">
                      @csrf
              @method('post')
                    <fieldset>
                      <div class="form-group">
                        
                        <input id="firstname" class="form-control" value="{{$user->email}}" name="email" type="hidden">
                      </div>
                      <div class="form-group">
                        <label for="firstname">Password</label>
                        <input id="firstname" class="form-control" name="password" type="password">
                      </div>
                       <div class="form-group">
                        <label for="firstname">Confirm Password</label>
                        <input id="firstname" class="form-control"  name="confirm-password" type="password">
                      </div>
                     
                      
                      <input class="btn btn-primary" type="submit" value="Submit">
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
      
</div>
@endsection