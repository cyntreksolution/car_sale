@extends('admin.layout.master')
@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Users 
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Users Details</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Details</li>
                    </ol>
                </nav>
            </div>
            <div class="card">
                <div class="card-body">

                    <h4 class="card-title">Users Details</h4>
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>Order #</th>
                                        <th>name</th>
                                        <th>email</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($users as $key => $user)
                                        <tr>

                                            <td>{{++$key}}</td>
                                            <td>{{$user->name}}</td>
                                            <td>{{$user->email}}</td>
                                         

                                            <td>
                                                <a href="{{route('users.edit',[$user->id])}}"><i
                                                        class="fas fa fa-key btn-icon-append fa-2x"> </i></a>
                                                
                                            </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        @endsection

        @section('script')
            <script src="../../js/data-table.js"></script>
            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
          
@endsection
