<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="profile-image">
                    <img src="{{asset('admin/images/faces/user.jpg')}}" alt="image"/>
                </div>
                <div class="profile-name">
                    <p class="name">
                        Welcome {{ Auth::user()->name }}
                    </p>
                    <p class="designation">
                        Super Admin
                    </p>
                </div>
            </div>
        </li>
{{--        <li class="nav-item">--}}
{{--            <a class="nav-link" href="{{url('dashboard')}}">--}}
{{--                <i class="fa fa-home menu-icon"></i>--}}
{{--                <span class="menu-title">Dashboard</span>--}}
{{--            </a>--}}
{{--        </li>--}}

        <li class="nav-item">
            <a class="nav-link" href="{{url('types')}}" aria-expanded="false" aria-controls="page-layouts">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Types</span>

            </a>

        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('models')}}" aria-expanded="false" aria-controls="page-layouts">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Models</span>

            </a>

        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('brands')}}" aria-expanded="false" aria-controls="page-layouts">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Brands</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('vehicles')}}" aria-expanded="false" aria-controls="page-layouts">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Vehcles</span>
            </a>
        </li>
         <li class="nav-item">
            <a class="nav-link" href="{{url('users')}}" aria-expanded="false" aria-controls="page-layouts">
                <i class="fab fa-trello menu-icon"></i>
                <span class="menu-title">Users</span>
            </a>
        </li>
    </ul>
</nav>
