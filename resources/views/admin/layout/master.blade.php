<!DOCTYPE html>
<html lang="en">


<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title> Admin</title>
  <!-- plugins:css -->
  @include('admin.layout.includes.style')
 
</head>
<body>
 @include('admin.layout.includes.header')
      <!-- partial -->
      <!-- partial:partials/_sidebar.html -->
       @include('admin.layout.includes.sidebar')
      <!-- partial -->
      @yield('content')
      
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        @include('admin.layout.includes.footer')
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
 @include('admin.layout.includes.javascript')
 @yield('script')
</body>


</html>
