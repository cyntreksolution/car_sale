@extends('admin.layout.master')
@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Vehicles
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Vehicles </a></li>
                        <li class="breadcrumb-item active" aria-current="page"> Create</li>
                    </ol>
                </nav>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Vehicles</h4>
                            <form class="cmxform" method="POST" action="{{ route('vehicles.store') }}"
                                  enctype="multipart/form-data">
                                @csrf
                                <fieldset>
                                    <div class="form-group row">
                                        <div class="col-md-4">
                                            <label for="firstname">Brand</label>
                                            <select class="form-control" required  name="brand">
                                                <option>Select Brand</option>
                                                @foreach ($brands as $key => $value)
                                                    <option value="{{ $key }}">
                                                        {{$value}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="firstname">Model</label>
                                            <select class="form-control" required  name="model">
                                                <option>Select Models</option>
                                                @foreach ($models as $key => $model)
                                                    <option value="{{ $key }}">
                                                        {{$model}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <label for="firstname">Types</label>
                                            <select class="form-control" required  name="type">
                                                <option>Select Types</option>
                                                @foreach ($types as $key => $type)
                                                    <option value="{{ $key }}">
                                                        {{$type}}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col">
                                            <label for="firstname">Title</label>
                                            <input  class="form-control" required name="title" type="text">
                                        </div>
                                        <div class="col">
                                            <label for="firstname">Price</label>
                                            <input  class="form-control" name="price" type="number">
                                        </div>
                                    </div>


                                    <div class="form-group row">
                                        <div class="col">
                                            <label for="firstname">Start Price</label>
                                            <input  class="form-control" name="start_price" type="number">
                                        </div>
                                        <div class="col">
                                            <label for="firstname">End Price</label>
                                            <input  class="form-control" name="end_price" type="number">
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label for="firstname">Overview</label>
                                            <textarea class="form-control" required name="overview"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label for="firstname">Additional Specification</label>
                                            <textarea class="form-control"  name="additional_spec"></textarea>
                                        </div>
                                    </div>
                                    <hr>


                                    <h4 class="card-title">Vehicles Specifications</h4>
                                    <div class="form-group row">
                                        <div class="col">
                                            <label for="firstname"> Mileage</label>
                                            <input  class="form-control" name="mileage" type="number">
                                        </div>
                                        <div class="col">
                                            <label for="firstname"> Transmission</label>
                                           {!! Form::select('transmission', ['Manual'=>'Manual','Triptonic'=>'Triptonic','Auto'=>'Auto'] , null , ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col">
                                            <label for="firstname"> Year</label>
                                            <input  class="form-control" name="year" type="number" maxlength="4">
                                        </div>
                                        <div class="col">
                                            <label for="firstname"> Fuel Type</label>
                                            {!! Form::select('fuel_type', ['Petrol'=>'Petrol','Diesel'=>'Diesel','Hybrid'=>'Hybrid'] , null , ['class' => 'form-control']) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <label for="firstname">Engine Capacity</label>
                                            <input  class="form-control" name="engine_capacity" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col">
                                            <label for="firstname">Colour</label>
                                            <input  class="form-control" name="color" type="text">
                                        </div>
                                        <div class="col">
                                            <label for="firstname"> Shape</label>
                                            <input  class="form-control" name="shape" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col">
                                            <label for="firstname">Doors</label>
                                            <input  class="form-control" name="doors" type="text">
                                        </div>
                                        <div class="col">
                                            <label for="firstname"> Seats</label>
                                            <input  class="form-control" name="seats" type="text">
                                        </div>
                                       

                                    </div>

                                     <div class="col">
                                            <input type="checkbox" id="featured" name="featured" value="1">
                                                <label for="Featured">Featured</label>
                                        </div>
                                    <hr>
                                    <div class="form-group">
                                        <label>Images upload</label>
                                        <input type="file" name="img[]" class="file-upload-default">
                                        <div class="input-group col-xs-12">
                                            <input required type="file" multiple="multiple" class="form-control file-upload-info" name="photos[]" placeholder="Upload Images">
                                        </div>
                                    </div>
                                    <input class="btn btn-primary" type="submit" value="Submit">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
@endsection
