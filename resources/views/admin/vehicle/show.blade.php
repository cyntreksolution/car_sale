@extends('admin.layout.master')
@section('content')
<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
                Vehicles 
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Vehicles</a></li>
                <li class="breadcrumb-item active" aria-current="page"> Details</li>
                </ol>
            </nav>
          </div>
            <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                  <h4 >Vehicles Details</h4><br>
                      <div class="form-group row" >
                        <div class="col-6 " > 
                        <label for="firstname" style="font-size: 18px;">Brand Name : &nbsp; {{$vehicle->brand->name}}</label>
                      </div>
                       
                       <div class="col-6 "> 
                        <label for="firstname" style="font-size: 18px;">Model Name : &nbsp; {{$vehicle->model->name}}</label>
                            
                      </div>
                    </div>

                     <div class="form-group row" >
                        <div class="col-6 " > 
                        <label for="firstname" style="font-size: 18px;">Type Name : &nbsp; {{$vehicle->type->name}}</label>
                      </div>
                       
                       <div class="col-6 "> 
                        <label for="firstname" style="font-size: 18px;">Title : &nbsp; {{$vehicle->title}}</label>
                            
                      </div>
                    </div>
                      
                     <div class="form-group row" >
                        <div class="col-6 " > 
                        <label for="firstname" style="font-size: 18px;">Price : &nbsp; {{$vehicle->price}}</label>
                      </div>
                       
                       <div class="col-6 "> 
                        <label for="firstname" style="font-size: 18px;">Start Price : &nbsp; {{$vehicle->start_price}}</label>
                            
                      </div>
                    </div>

                     <div class="form-group row" >
                        <div class="col-6 " > 
                        <label for="firstname" style="font-size: 18px;">End Price : &nbsp; {{$vehicle->end_price}}</label>
                      </div>
                       
                       <div class="col-6 "> 
                        <label for="firstname" style="font-size: 18px;">Overview : &nbsp; {{$vehicle->overview}}</label>
                            
                      </div>
                    </div>
                    <hr>
                        <h4>Specification</h4><br>
                    <div class="form-group row" >
                        <div class="col-6 " > 
                        <label for="firstname" style="font-size: 18px;">Mileage : &nbsp; {{$vehicle->specification->mileage}}</label>
                      </div>
                       
                       <div class="col-6 "> 
                        <label for="firstname" style="font-size: 18px;">Transmission : &nbsp; {{$vehicle->specification->transmission}}</label>
                            
                      </div>
                    </div>
                      
                    <div class="form-group row" >
                        <div class="col-6 " > 
                        <label for="firstname" style="font-size: 18px;">Year : &nbsp; {{$vehicle->specification->year}}</label>
                      </div>
                       
                       <div class="col-6 "> 
                        <label for="firstname" style="font-size: 18px;">Fuel Type : &nbsp; {{$vehicle->specification->fuel_type}}</label>
                            
                      </div>
                    </div>

                    <div class="form-group row" >
                        <div class="col-6 " > 
                        <label for="firstname" style="font-size: 18px;">Engine Capacity : &nbsp; {{$vehicle->specification->engine_capacity}}</label>
                      </div>
                       
                       <div class="col-6 "> 
                        <label for="firstname" style="font-size: 18px;">Colour : &nbsp; {{$vehicle->specification->color}}</label>
                            
                      </div>
                    </div>

                      <div class="form-group row" >
                        <div class="col-6 " > 
                        <label for="firstname" style="font-size: 18px;">Shape : &nbsp; {{$vehicle->specification->shape}}</label>
                      </div>
                       
                       <div class="col-6 "> 
                        <label for="firstname" style="font-size: 18px;">Doors : &nbsp; {{$vehicle->specification->doors}}</label>
                            
                      </div>
                    </div>

                     <div class="form-group row" >
                        <div class="col-6 " > 
                        <label for="firstname" style="font-size: 18px;">Seats : &nbsp; {{$vehicle->specification->seats}}</label>
                      </div>
                       
                      
                    </div>
                    <h4>Images</h4>
                     <div class="form-group row" >
                      @foreach ($vehicle->images as $image)
                    <img width="150px" src="{{asset('uploads/vehicles/'.$image->file_name)}}" alt="" />
                    @endforeach
                  </div>
                    </fieldset>
                </div>
              </div>
            </div>
          </div>
      
</div>
@endsection