@extends('admin.layout.master')
@section('content')
<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
                Vehicles 
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Vehicles </a></li>
                <li class="breadcrumb-item active" aria-current="page"> Edit</li>
                </ol>
            </nav>
          </div>
            <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Vehicles</h4>

                   {!! Form::model($vehicle, ['route' => ['vehicles.update', $vehicle->id], 'method' => 'PATCH','files'=>true]) !!}

                
                      @csrf 
                      @method('PATCH')
                    <fieldset>
                      <div class="form-group row">
                        <div class="col">
                        <label for="firstname">Brand</label>

                      {!!Form::select('brand_id', $brands, null, ['placeholder' => 'Select Brand.','class'=>'form-control'])!!}                        

                        
                         </div>
                         <div class="col">
                        <label for="firstname">Model</label>
                         {!!Form::select('model_id', $models, null, ['placeholder' => 'Select Model','class'=>'form-control'])!!}   
                      
                         </div>
                      </div>
                      <div class="form-group row">
                        <div class="col">
                        <label for="firstname">Types</label>
                         {!!Form::select('type_id', $types, null, ['placeholder' => 'Select Type','class'=>'form-control'])!!}   
                        
                         </div>
                         <div class="col">
                        <label for="firstname">Title</label>
                        {!!Form::text('title', null, ['placeholder' => 'Select Type','class'=>'form-control'])!!}
                        
                      </div>
                    </div>
                       <div class="form-group row">
                        <div class="col">
                        <label for="firstname">Price</label>
                         {!!Form::text('price', null, ['class'=>'form-control'])!!}
                        
                         </div>
                         <div class="col">
                        <label for="firstname">Start Price</label>
                        {!!Form::text('start_price', null, ['class'=>'form-control'])!!}
                       
                         </div>
                      </div>
                       <div class="form-group row">
                        <div class="col">
                        <label for="firstname">End Price</label>
                         {!!Form::text('end_price', null, ['class'=>'form-control'])!!}
                      
                         </div>
                         <div class="col">
                        <label for="firstname">Over view</label>
                         {!!Form::text('overview', null, ['class'=>'form-control'])!!}
                        
                         </div>
                      </div>
                     <hr>
                    
                        <h4 class="card-title">Vehicles Specifications</h4>
                      <div class="form-group row">
                        <div class="col">
                        <label for="firstname"> Mileage</label>
                         {!!Form::text('mileage',$vehicle->specification->mileage, ['class'=>'form-control'])!!}
                       
                         </div>
                         <div class="col">
                        <label for="firstname"> Transmission</label>
                        {!!Form::text('transmission', $vehicle->specification->transmission, ['class'=>'form-control'])!!}
                        
                         </div>
                      </div>
                        <div class="form-group row">
                        <div class="col">
                        <label for="firstname">   Year</label>
                        {!!Form::text('year', $vehicle->specification->year, ['class'=>'form-control'])!!}
                       </div>
                         <div class="col">
                        <label for="firstname"> Fuel Type</label>
                        {!!Form::text('fuel_type', $vehicle->specification->fuel_type, ['class'=>'form-control'])!!}
                       
                         </div>
                      </div>
                       <div class="form-group row">
                        <div class="col-md-12">
                        <label for="firstname">Engine Capacity</label>
                        {!!Form::text('engine_capacity', $vehicle->specification->engine_capacity, ['class'=>'form-control'])!!}
                        
                         </div>
                        
                      </div>

                      <div class="form-group row">
                        <div class="col">
                        <label for="firstname">Colour</label>
                        {!!Form::text('color', $vehicle->specification->color, ['class'=>'form-control'])!!}
                        
                         </div>
                         <div class="col">
                        <label for="firstname"> Shape</label>
                        {!!Form::text('shape', $vehicle->specification->shape, ['class'=>'form-control'])!!}
                       
                         </div>
                      </div>
                      <div class="form-group row">
                        <div class="col">
                        <label for="firstname">Doors</label>
                        {!!Form::text('doors', $vehicle->specification->doors, ['class'=>'form-control'])!!}
                        
                         </div>
                         <div class="col">
                        <label for="firstname"> Seats</label>
                        {!!Form::text('seats', $vehicle->specification->seats, ['class'=>'form-control'])!!}
                       
                         </div>
                      </div>

                      <div class="col">
                                               {!! Form::checkbox('featured', '1', null,  ['id' => 'featured','class'=>'form-check-input']) !!}
            Featured
                                        </div>

                       <br>
                        
                    <div class="form-group">
                     <div style="float: left">
                      @foreach ($vehicle->images as $image)
                    @if($image!='')
                        <img width="150px" src="{{asset('uploads/vehicles/'.$image->file_name)}}" alt="" />
<a href="" data-id="{{$image->id}}" class="button delete-confirm"><i style="color: #e52d27;" class="remove fa fa-times-circle fa-2x"> </i></a>
                    @endif

                     @endforeach
                </div>
              </div>
                       <div class="form-group">

                      <!-- <label>Images upload</label> -->
                      <input type="file" name="img[]" class="file-upload-default">
                      <div class="input-group col-xs-12">
                        <input type="file" multiple="multiple" class="form-control file-upload-info" name="photos[]" placeholder="Upload Images">
                         
                      </div>
                    </div>
                   

                      <input class="btn btn-primary" type="submit" value="Submit">
                    </fieldset>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
      
</div>
@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
  $('.delete-confirm').on('click', function () {
        // return confirm('Are you sure want to delete?');
        event.preventDefault();//this will hold the url
        var id = $(this).data('id');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
                if (result.value) {
               $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

               $.ajax({
                type: 'post',
                url: '/vehicles/imagedestroy/',
                data: {
                    id: id
                },
                success: function (success) {
                    if (success) {
                        Swal.fire(
                            'Deleted!',
                            'Your record has been deleted.',
                            'success'
                        );
                        location.reload();
                    } else {
                        let msg = result.message;
                        console.log('msg');
                    }
                }
        });
    }
  })
});
</script>
@endsection
