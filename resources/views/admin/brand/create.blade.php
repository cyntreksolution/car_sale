@extends('admin.layout.master')
@section('content')
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="page-header">
                <h3 class="page-title">
                    Vehicles Brand
                </h3>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Vehicles Brand</a></li>
                        <li class="breadcrumb-item active" aria-current="page"> Brand</li>
                    </ol>
                </nav>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Vehicles Brand</h4>
                            <form class="cmxform" method="POST" action="{{ route('brands.store') }}"
                                  enctype="multipart/form-data">
                                @csrf
                                <fieldset>
                                    <div class="form-group">
                                        <label for="firstname">Name</label>
                                        <input id="firstname" class="form-control" name="name" type="text">
                                    </div>
                                    <div class="form-group">
                                        <label for="lastname">Logo</label>
                                        <input type="file" id="exampleInputFile" name="logo">
                                    </div>
                                     <div class="col">
                                            <input type="checkbox" id="featured" name="featured" value="1">
                                                <label for="Featured">Featured</label>
                                        </div>

                                    <input class="btn btn-primary" type="submit" value="Submit">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
@endsection
