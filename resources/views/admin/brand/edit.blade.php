@extends('admin.layout.master')
@section('content')
<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
                Vehicles Brand
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Vehicles Brand</a></li>
                <li class="breadcrumb-item active" aria-current="page"> Brand</li>
                </ol>
            </nav>
          </div>
            <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Vehicles Brand</h4>
                  <form class="cmxform"  method="POST" action="{{ route('brands.update',$brand->id) }}" enctype="multipart/form-data">
                      @csrf
                      @method('PATCH')
                    <fieldset>
                      <div class="form-group">
                        <label for="firstname">Name</label>
                        <input id="firstname" class="form-control" value="{{$brand->name}}" name="name" type="text">
                      </div>
                       @if (!empty($brand->logo))
                      <div class="form-group ">
                          <img  id="upload-image" width="100px" src="{{asset('uploads/brand/'.$brand->logo)}}" alt="" />
                      </div>
                      @endif
                      <div class="form-group">
                      <label for="exampleInputFile">Logo</label>
                      <input type="file" id="exampleInputFile" name="logo">

                      
                    </div>
                     <div class="form-group">
                      {!! Form::checkbox('featured', '1', null,  ['id' => 'featured','class'=>'form-check-input']) !!}
                                              Featured
                                        </div>
                      
                      
                      <input class="btn btn-primary" type="submit" value="Submit">
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
      
</div>
@endsection