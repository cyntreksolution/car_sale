@extends('admin.layout.master')
@section('content')
<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
                Vehicles Types
            </h3>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Vehicles Types</a></li>
                <li class="breadcrumb-item active" aria-current="page"> Types</li>
                </ol>
            </nav>
          </div>
            <div class="row">
            <div class="col-lg-12">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Vehicles Types</h4>
                  <form class="cmxform"  method="POST" action="{{ route('types.update',$type->id) }}">
                      @csrf
              @method('PATCH')
                    <fieldset>
                      <div class="form-group">
                        <label for="firstname">Name</label>
                        <input id="firstname" class="form-control" value="{{$type->name}}" name="name" type="text">
                      </div>
                      <div class="form-group">
                        <label for="lastname">Description</label>
                        <textarea id="lastname" rows="5" class="form-control" name="description" type="text">{{$type->description}}</textarea>
                      </div>
                      
                      <input class="btn btn-primary" type="submit" value="Submit">
                    </fieldset>
                  </form>
                </div>
              </div>
            </div>
          </div>
      
</div>
@endsection