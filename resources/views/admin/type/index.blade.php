@extends('admin.layout.master')
@section('content')
<div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              Vehicle Types
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Vehicle Types</a></li>
                <li class="breadcrumb-item active" aria-current="page">Types</li>
              </ol>
            </nav>
          </div>
          <div class="card">
            <div class="card-body">
            
              <h4 class="card-title">Vehicle Types</h4>
                <div class="row">
                <div class="col-12">
                  <div class="table-responsive">
                    <table id="order-listing" class="table">
                      <thead>
                        <tr>
                            <th>Order #</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($types as $key => $type)
                        <tr>
                           
                            <td>{{++$key}}</td>
                            <td>{{$type->name}}</td>
                            <td>{{$type->description}}</td>
                            
                            <td>
                              <a href="{{route('types.edit',[$type->id])}}"><i class="fas fa-pencil-alt btn-icon-append fa-2x"> </i></a>
                              <a href="" data-id="{{$type->id}}" class="button delete-confirm"><i style="color: #e52d27;" class="remove fa fa-times-circle fa-2x"> </i></a>
                            </td>
                            
                        </tr>
                         @endforeach
                      </tbody>
                    </table>

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <a href="{{ route('types.create')}}"><div id="settings-trigger"><i class="fas fa-plus-circle fa-10x"></i></div></a>
@endsection

@section('script')
<script src="../../js/data-table.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript">
  $('.delete-confirm').on('click', function () {
        // return confirm('Are you sure want to delete?');
        event.preventDefault();//this will hold the url
        var id = $(this).data('id');
        Swal.fire({
          title: 'Are you sure?',
          text: "You won't be able to revert this!",
          icon: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
                if (result.value) {
               $.ajaxSetup({
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
               
               $.ajax({
                type: "delete",
                url: '/types/'+id,
                data: {
                    id: id
                },
                success: function (success) {
                    if (success) {
                        Swal.fire(
                            'Deleted!',
                            'Your record has been deleted.',
                            'success'
                        );
                        location.reload();
                    } else {
                        let msg = result.message;
                        console.log('msg');
                    }
                }
        });
    }
  })
});
</script>
@endsection