@extends('layouts.master')

@section('content')
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title">
                        <h2>Order Now</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN Main Container col2-right -->
    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <div class="col-main col-sm-9 wow bounceInUp animated animated" style="visibility: visible;">
                    <div id="messages_product_view"></div>
                    <form action="{{ action('HomePageController@ordernowstore') }}" method="post">
                        @csrf
                        <div class="static-contain">
                            <fieldset class="group-select">
                                <ul>
                                    <li id="billing-new-address-form">
                                        <fieldset class="">
                                            <ul>
{{--                                                <li>--}}
{{--                                                    <div class="customer-name">--}}
{{--                                                        <div class="input-box name-firstname">--}}
{{--                                                            <label for="name"><em class="required">*</em>Brand</label>--}}
{{--                                                            <br>--}}
{{--                                                            <select name="brand_id" id="brand_id" title="brand_id"--}}
{{--                                                                    class="input-text required-entry" type="text"--}}
{{--                                                                    required>--}}
{{--                                                                <option value="">Select Brand</option>--}}
{{--                                                                @foreach ($brands as $key => $value)--}}
{{--                                                                    <option value="{{ $key }}">--}}
{{--                                                                        {{$value}}--}}
{{--                                                                    </option>--}}
{{--                                                                @endforeach--}}
{{--                                                            </select>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="input-box name-firstname">--}}
{{--                                                            <label for="email"><em class="required">*</em>Model</label>--}}
{{--                                                            <br>--}}
{{--                                                            <select name="model_id" id="model_id" title="model_id"--}}
{{--                                                                    class="input-text required-entry validate-email"--}}
{{--                                                                    type="text" required>--}}
{{--                                                                <option value="">Select Models</option>--}}
{{--                                                                @foreach ($models as $key => $model)--}}
{{--                                                                    <option value="{{ $key }}">--}}
{{--                                                                        {{$model}}--}}
{{--                                                                    </option>--}}
{{--                                                                @endforeach--}}
{{--                                                            </select>--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </li>--}}
                                                <li>
                                                    <div class="customer-name">
                                                        <div class="input-box name-firstname">
                                                            <label for="email"><em class="required">*</em>Vehicle</label>
                                                            <br>
                                                            <select name="vehicle_id" id="vehicle_id"
                                                                    class="input-text required-entry validate-email"
                                                                    type="text" required>
                                                                <option value="">Select Vehicle</option>
                                                                <option value="0">Not Listed</option>
                                                                @foreach ($vehicles as $key => $vehicle)
                                                                    <option value="{{ $vehicle->id }}">{{$vehicle->brand->name.' '.$vehicle->title}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="input-box name-firstname">
                                                            <label for="name"><em class="required">*</em>Name</label>
                                                            <br>
                                                            <input name="name" id="name" title="Name"
                                                                   class="input-text required-entry" type="text"
                                                                   required>
                                                        </div>

                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="input-box name-firstname">
                                                        <label for="email"><em class="required">*</em>Email</label>
                                                        <br>
                                                        <input name="email" id="email" title="Email"
                                                               class="input-text required-entry validate-email"
                                                               type="text" required>
                                                    </div>
                                                    <div class="input-box name-firstname">
                                                        <label for="telephone">Telephone</label>
                                                        <br>
                                                        <input name="telephone" id="telephone" title="Telephone"
                                                               value="" class="input-text" type="text">
                                                    </div>
                                                </li>
                                                <li>
                                                    <label for="comment"><em class="required">*</em>Comment</label>
                                                    <br>
                                                    <textarea name="comment" id="comment" title="Comment"
                                                              class="required-entry input-text" cols="5" rows="3"
                                                              required></textarea>
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </li>

                                    <input type="text" name="hideit" id="hideit" value=""
                                           style="display:none !important;">
                                    <div class="buttons-set">
                                        <button type="submit" title="Submit" class="button submit">
                                            <span><span>Submit</span></span></button>
                                    </div>
                                </ul>
                            </fieldset>
                        </div>
                    </form>

                </div>

                <!--col-right sidebar-->
            </div>
            <!--row-->
        </div>
        <!--main-container-inner-->
    </div>
    <!--main-container col2-left-layout-->



    </div>
@endsection
