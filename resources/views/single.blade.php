@extends('layouts.master')

@section('content')
    <div class="page-heading">
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                    </div>
                    <!--col-xs-12-->
                </div>
                <!--row-->
            </div>
            <!--container-->
        </div>
        <div class="page-title">
            <h2>PRODUCT Detail</h2>
        </div>
    </div>

    <div class="main-container col1-layout wow bounceInUp animated">
        <div class="main">
            <div class="col-main">
                <div class="product-view wow bounceInUp animated">
                    <div id="messages_product_view"></div>
                    <div class="product-essential container">
                        <div class="row">
                            <div class="product-img-box col-lg-5 col-sm-5 col-xs-12">
                                <div class="product-image">
                                    <div class="product-full">
                                        <img id="product-zoom1"
                                             src="{{asset('uploads/vehicles/'.$vehicle->images[0]->file_name)}}"
                                             data-zoom-image="{{asset('uploads/vehicles/'.$vehicle->images[0]->file_name)}}"
                                             alt="product-image"/>
                                    </div>
                                    <div class="more-views">
                                        <div class="slider-items-products">
                                            <div id="gallery_02"
                                                 class="product-flexslider hidden-buttons product-img-thumb">
                                                <div class="slider-items slider-width-col4 block-content">
                                                    @foreach($vehicle->images as $key => $image)
                                                        <div class="more-views-items">
                                                            <a href="#"
                                                               data-image="{{asset('uploads/vehicles/'.$image->file_name)}}"
                                                               data-zoom-image="{{asset('uploads/vehicles/'.$image->file_name)}}">

                                                                <img id="product-zoom1"
                                                                     src="{{asset('uploads/vehicles/'.$image->file_name)}}"
                                                                     alt="product-image"/>
                                                            </a>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="toll-free"><a href="tel:00447859178849"><i class="fa fa-phone"></i> 0044 785
                                        917 8849</a>
                                </div>
                            </div>


                            <div class="product-shop col-lg- col-sm-7 col-xs-12">
                                <div class="brand">{{$vehicle->brand->name}}</div>
                                <div class="product-name">
                                    <h1>{{$vehicle->title}}</h1>
                                </div>

                                <div class="price-block">
                                    <div class="price-box">
                                        <p class="special-price"><span class="price-label">Special Price</span>
                                            @if($vehicle->price)
                                                <span id="product-price-48"
                                                      class="price">{{'$ '.$vehicle->price}}</span>
                                            @else
                                                <span id="product-price-48"
                                                      class="price">{{($vehicle->start_price)?'$ '.$vehicle->start_price.' - '.'$ '.$vehicle->end_price:'Negotiable'}}</span>

                                            @endif
                                        </p>

                                    </div>
                                </div>

                                <div class="spec-row" id="summarySpecs">
                                    <h2>Overview</h2>
                                    <table width="100%">
                                        <tbody>
                                        <tr>
                                            <td class="label-spec"> Mileage <span class="coln">:</span></td>
                                            <td class="value-spec">{{$vehicle->specification->mileage}} km</td>
                                        </tr>
                                        <tr>
                                            <td class="label-spec"> Engine Capacity <span class="coln">:</span></td>
                                            <td class="value-spec"> {{$vehicle->specification->engine_capacity}}
                                            </td>
                                        </tr>
                                        <tr class="odd">
                                            <td class="label-spec"> Transmission <span class="coln">:</span></td>
                                            <td class="value-spec"> {{$vehicle->specification->transmission}}</td>
                                        </tr>
                                        <tr class="odd">
                                            <td class="label-spec"> Fuel Type <span class="coln">:</span></td>
                                            <td class="value-spec"> {{$vehicle->specification->fuel_type}}</td>
                                        </tr>
                                        <tr>
                                            <td class="label-spec"> Model <span class="coln">:</span></td>
                                            <td class="value-spec"> {{$vehicle->model->name}}</td>
                                        </tr>
                                        <tr>
                                            <td class="label-spec"> Colour <span class="coln">:</span></td>
                                            <td class="value-spec"> {{$vehicle->specification->color}}</td>
                                        </tr>
                                        <tr class="odd">
                                            <td class="label-spec"> Shape <span class="coln">:</span></td>
                                            <td class="value-spec"> {{$vehicle->specification->shape}}</td>
                                        </tr>
                                        <tr>
                                            <td class="label-spec"> Year <span class="coln">:</span></td>
                                            <td class="value-spec"> {{$vehicle->specification->year}}</td>
                                        </tr>
                                        <tr class="odd">
                                            <td class="label-spec"> Doors <span class="coln">:</span></td>
                                            <td class="value-spec">{{$vehicle->specification->doors}}</td>
                                        </tr>
                                        <tr>
                                            <td class="label-spec"> Seats <span class="coln">:</span></td>
                                            <td class="value-spec">{{$vehicle->specification->seats}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="product-collateral container">
                        <ul id="product-detail-tab" class="nav nav-tabs product-tabs">
                            <li class="active">
                                <a href="#product_tabs_description" data-toggle="tab"> Standard Specifications </a>
                            </li>
                            <li class="">
                                <a href="#product_tabs_add" data-toggle="tab"> Additional Specifications </a>
                            </li>
                        </ul>
                        <div id="productTabContent" class="tab-content">
                            <div class="tab-pane fade in active" id="product_tabs_description">
                                <div class="std">
                                    <p>{{$vehicle->overview}}</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="product_tabs_add">
                                <div class="std">
                                    <p>{{$vehicle->additional_spec}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="box-additional">
                        <!-- BEGIN RELATED PRODUCTS -->
                        <div class="related-pro container">
                            <div class="slider-items-products">
                                <div class="new_title center">
                                    <h2>Related Products</h2>
                                </div>
                                <div id="related-slider" class="product-flexslider hidden-buttons">
                                    <div class="slider-items slider-width-col4 products-grid">
                                        @foreach($related_products as $key => $vehicle)

                                            <div class="item">

                                                <div class="item-inner">

                                                    <div class="item-img">
                                                        <div class="item-img-info">
                                                            <a href="{{route('single.car',[$vehicle->id])}}"
                                                               title="Retis lapen casen"
                                                               class="product-image">
                                                                <img style="max-height: 130px" src="{{asset('uploads/vehicles/'.$vehicle->images[0]->file_name)}}" alt="Retis lapen casen"></a>
                                                            <div class="item-box-hover">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="item-info">
                                                        <div class="info-inner">

                                                            <div class="item-title">
                                                                <a href="{{route('single.car',[$vehicle->id])}}"
                                                                   title="Retis lapen casen">{{$vehicle->title}}</a>
                                                            </div>

                                                            <div class="item-content">
                                                                 <div class="item-price">
                                                                    <div class="price-box">
                                                                        <span class="regular-price">
                                                                            @if ($vehicle->price)
                                                                                <span class="price">{{'$ '.$vehicle->price}}</span>
                                                                                @else
                                                                                <span class="price">{{($vehicle->start_price)?'$ '.$vehicle->start_price.' - '.'$ '.$vehicle->end_price:'Negotiable'}}</span>
                                                                            @endif

                                                                        </span>
                                                                    </div>
                                                                </div>

                                                                <div class="other-info">
                                                                    <div class="col-km">
                                                                        <i class="fa fa-tachometer"></i>{{optional($vehicle->specification)->mileage}}km
                                                                    </div>
                                                                    <div class="col-engine">
                                                                        <i class="fa fa-gear"></i> {{optional($vehicle->specification)->transmission}}
                                                                    </div>
                                                                    <div class="col-date">
                                                                        <i class="fa fa-calendar" aria-hidden="true"></i> {{optional($vehicle->specification)->year}}
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end related product -->

                    </div>
                    <!-- end related product -->
                </div>
                <!--box-additional-->
                <!--product-view-->
            </div>
        </div>
        <!--col-main-->
    </div>

@endsection
