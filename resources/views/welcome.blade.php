@extends('layouts.master')

@section('content')
    <div class="content">
        <div class="container">
            <h5 class="tp-caption text-black-50 LargeTitle f-46 sfl  tp-resizeme" style="letter-spacing: 1px;white-space: nowrap; color: black !important;margin-top: 15px">A & S Imports and Exports LTD</h5>
        </div>
        @include('home.slider')
        {{--                @include('home.types')--}}

        <div id="top" class="mt-5" style="margin-top: 25px">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <a href="{{route('order-now')}}" data-scroll-goto="1">
                            <img src="images/speakers.png" alt="promotion-banner1">
                        </a>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <a href="{{route('aboutus')}}" data-scroll-goto="2">
                            <img src="images/schedule.png" alt="promotion-banner2">
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @include('home.best')
{{--        @include('home.deals')--}}
        @include('home.testimonials')
    </div>

    {{--    @include('home.best')--}}
    <div class="logo-brand container">
        <div class="brand-title">
            <h2>Popular Brands</h2>
        </div>
        <div class="slider-items-products">
            <div id="brand-slider" class="product-flexslider hidden-buttons">
                <div class="slider-items slider-width-col6">

                    @foreach($brands as $brand)
                        <div class="item">
                            <div class="logo-item">
                                <a href="/listing?brand={{$brand->id}}">
                                    <img class="brand-x" src="{{asset('uploads/brand/'.$brand->logo)}}" alt="Image">
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
