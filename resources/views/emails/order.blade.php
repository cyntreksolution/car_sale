@component('mail::message')
# Order Details
@if ($order->vehicle_id == 0)
Vehicle : Not Selected
@else
Vehicle : {{\App\Vehicle::find($order->vehicle_id)->title}}
@endif
<p>Name : {{$order->name}}</p>
<p>Email : {{$order->email}}</p>
<p>Telephone : {{$order->telephone}}</p>
<p>Comment : {{$order->comment}}</p>


Thanks,<br>
{{ config('app.name') }}
@endcomponent
