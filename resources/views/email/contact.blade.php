@component('mail::message')
# New Contact Information

<p>Name : {{$contact->name}}</p>
<p>Email : {{$contact->email}}</p>
<p>Telephone : {{$contact->telephone}}</p>
<p>Comment : {{$contact->comment}}</p>



Thanks,<br>
{{ config('app.name') }}
@endcomponent
