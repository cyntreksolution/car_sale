@extends('layouts.master')

@section('content')
    <div class="page-heading">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title">
                        <h2>Contact Us</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN Main Container col2-right -->
    <div class="main-container col2-right-layout">
        <div class="main container">
            <div class="row">
                <div class="col-main col-sm-9 wow bounceInUp animated animated" style="visibility: visible;">
                    <div id="messages_product_view"></div>
                    <form action="{{ action('HomePageController@contactusstore') }}" method="post">
                        @csrf
                        <div class="static-contain">
                            <fieldset class="group-select">
                                <ul>
                                    <li id="billing-new-address-form">
                                        <fieldset class="">
                                            <ul>
                                                <li>
                                                    <div class="customer-name">
                                                        <div class="input-box name-firstname">
                                                            <label for="name"><em class="required">*</em>Name</label>
                                                            <br>
                                                            <input name="name" id="name" title="Name"
                                                                   class="input-text required-entry" type="text"
                                                                   required>
                                                        </div>
                                                        <div class="input-box name-firstname">
                                                            <label for="email"><em class="required">*</em>Email</label>
                                                            <br>
                                                            <input name="email" id="email" title="Email"
                                                                   class="input-text required-entry validate-email"
                                                                   type="text" required>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li>
                                                    <label for="telephone">Telephone</label>
                                                    <br>
                                                    <input name="telephone" id="telephone" title="Telephone" required value=""
                                                           class="input-text" type="number">
                                                </li>
                                                <li>
                                                    <label for="comment"><em class="required">*</em>Comment</label>
                                                    <br>
                                                    <textarea name="comment" id="comment" title="Comment"
                                                              class="required-entry input-text" cols="5" rows="3"
                                                              required></textarea>
                                                </li>
                                            </ul>
                                        </fieldset>
                                    </li>

                                    <input type="text" name="hideit" id="hideit" value=""
                                           style="display:none !important;">
                                    <div class="buttons-set">
                                        <button type="submit" title="Submit" class="button submit">
                                            <span><span>Submit</span></span></button>
                                    </div>
                                </ul>
                            </fieldset>
                        </div>
                    </form>

                </div>

                <aside class="col-right sidebar col-sm-3 wow bounceInUp animated __web-inspector-hide-shortcut__"
                       style="visibility: visible;">
                    <div class="block block-company">
                        <div class="block-title">Company</div>
                        <div class="block-content">
                            <ol id="recently-viewed-items">
                                <li class="item odd"><a>No 89,</a></li>
                                <li class="item even"><a> Abbey Park Road,</a></li>
                                <li class="item  odd"><a>Leicester,</a></li>
                                <li class="item last"><a>LE4 5AP UK</a></li>
                            </ol>
                        </div>
                        <div class="block-content">
                            <ol id="recently-viewed-items">
                                <li class="item odd"><a href="tel:00441162662153"> 0044 116 266 2153</a></li>
                                <li class="item odd"><a href="tel:00447859178849"> 0044 785 917 8849</a></li>
                                <li class="item odd"><span><a href="#">	info@as-exports.co.uk</a></span></li>
                            </ol>
                        </div>
                    </div>
                </aside>

                <!--col-right sidebar-->
            </div>
            <!--row-->
        </div>
        <!--main-container-inner-->
    </div>
    <!--main-container col2-left-layout-->



    </div>
@endsection
