<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>A & S Imports and Exports Ltd</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Default Description">
    <meta name="keywords" content="fashion, store, E-commerce">
    <meta name="robots" content="*">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="icon" href="#" type="image/x-icon">
    <link rel="shortcut icon" href="#" type="image/x-icon">

    <!-- CSS Style -->
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheet/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheet/font-awesome.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheet/bootstrap-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheet/revslider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheet/owl.carousel.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheet/owl.theme.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheet/jquery.bxslider.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheet/jquery.mobile-menu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheet/style.css')}}" media="all">
    <link rel="stylesheet" type="text/css" href="{{asset('stylesheet/responsive.css')}}" media="all">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,400,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Teko:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Saira+Condensed:300,400,500,600,700,800" rel="stylesheet">


    <style>
        .logo-x {
            width: 250px;
            /* height: 150px; */
            margin-top: -20px;
            margin-left: 0px;
        }

        .f-46 {
            font-size: 46px !important;
        }

        .brand-x {
            width: 70px !important;
            height: auto;
        }

        .logo-item {
            border: none !important;
        }

.fa {

  font-size: 25px;
  text-align: center;
  text-decoration: none;
  margin: 5px 2px;
  border-radius: 50%;
}

    </style>
</head>
<body>
<div id="page">
    <header>
        <div class="container">
            <div class="row">
                <div id="header">
                    <div class="header-container">
                        <div class="header-logo"><a href="/" title="Car HTML" class="logo">
                                <div>
                                    <img class="logo-x" src="{{asset('images/slider/logo.png')}}" alt="Car Store">
                                </div>
                            </a></div>
                        <div class="header__nav">
                            <div class="header-banner">
                                <div class="col-lg-6 col-xs-12 col-sm-6 col-md-6">
                                    {{--                                    <div class="assetBlock">--}}
                                    {{--                                        <div style="height: 20px; overflow: hidden;" id="slideshow">--}}
                                    {{--                                            <p style="display: block;">Hot days! - <span>50%</span> Get ready for--}}
                                    {{--                                                summer! </p>--}}
                                    {{--                                            <p style="display: none;">Save up to <span>40%</span> Hurry limited offer!--}}
                                    {{--                                            </p>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                </div>
                                <div class="col-lg-6 col-lg-6 col-xs-12 col-sm-6 col-md-6 call-us"><i
                                        class="fa fa-clock-o">
                                    </i> Mon - Fri : 09am to 06pm <i class="fa fa-phone">
                                    </i> <a style="color: white !important;" class="text-white"
                                            href="tel:00447859178849"> 0044 785 917 8849</a></div>
                            </div>

                            <div class="fl-nav-menu">
                                <nav>
                                    <div class="mm-toggle-wrap">
                                        <div class="mm-toggle"><i class="fa fa-bars"></i><span
                                                class="mm-label">Menu</span></div>
                                    </div>
                                    <div class="nav-inner">
                                        <!-- BEGIN NAV -->
                                        <ul id="nav" class="hidden-xs">

                                            <li class="active"><a class="level-top" href="{{route('contactus')}}"><span>Contact Us</span></a>
                                            </li>
                                            <li class="active"><a class="level-top" href="{{route('aboutus')}}"><span>About Us</span></a>
                                            </li>
                                            <li class="active"><a class="level-top" href="{{route('order-now')}}"><span>Order Now</span></a>
                                            </li>
                                            <li class="active"><a class="level-top"
                                                                  href="/listing"><span>Our stock</span></a>
                                            </li>
                                            <li class="active"><a class="level-top" href="/"><span>Home</span></a></li>
                                        </ul>
                                        <!--nav-->
                                    </div>

                                </nav>
                            </div>
                        </div>

                        <!--row-->

                    </div>
                </div>
            </div>
        </div>
    </header>

    @yield('content')

    <footer>
        <div class="footer-inner">
            {{--            <div class="our-features-box wow bounceInUp animated animated">--}}
            {{--                <div class="container">--}}
            {{--                    <ul>--}}
            {{--                        <li>--}}
            {{--                            <div class="feature-box">--}}
            {{--                                <div class="icon-truck"><img src="{{asset('images/world-icon.png')}}" alt="Image"></div>--}}
            {{--                                <div class="content">--}}
            {{--                                    <h6>World's #1</h6>--}}
            {{--                                    <p>Largest Auto portal</p>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <div class="feature-box">--}}
            {{--                                <div class="icon-support"><img src="{{asset('images/car-sold-icon.png')}}" alt="Image">--}}
            {{--                                </div>--}}
            {{--                                <div class="content">--}}
            {{--                                    <h6>Car Sold</h6>--}}
            {{--                                    <p>Every 4 minute</p>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </li>--}}
            {{--                        <li>--}}
            {{--                            <div class="feature-box">--}}
            {{--                                <div class="icon-money"><img src="{{asset('images/tag-icon.png')}}" alt="Image"></div>--}}
            {{--                                <div class="content">--}}
            {{--                                    <h6>Offers</h6>--}}
            {{--                                    <p>Stay updated pay less</p>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </li>--}}
            {{--                        <li class="last">--}}
            {{--                            <div class="feature-box">--}}
            {{--                                <div class="icon-return"><img src="{{asset('images/compare-icon.png')}}" alt="Image">--}}
            {{--                                </div>--}}
            {{--                                <div class="content">--}}
            {{--                                    <h6>Best Brands</h6>--}}
            {{--                                    <p>Decode the right car</p>--}}
            {{--                                </div>--}}
            {{--                            </div>--}}
            {{--                        </li>--}}
            {{--                    </ul>--}}
            {{--                </div>--}}
            {{--            </div>--}}
            {{--            <div class="newsletter-row">--}}
            {{--                <div class="container">--}}
            {{--                    <div class="row">--}}

            {{--                        <!-- Footer Newsletter -->--}}
            {{--                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col1">--}}
            {{--                            <div class="newsletter-wrap">--}}
            {{--                                <h5>Newsletter</h5>--}}
            {{--                                <h4>Get Notified Of any Updates!</h4>--}}
            {{--                                <form action="#" method="post" id="newsletter-validate-detail1">--}}
            {{--                                    <div id="container_form_news">--}}
            {{--                                        <div id="container_form_news2">--}}
            {{--                                            <input type="text" name="email" id="newsletter1"--}}
            {{--                                                   title="Sign up for our newsletter"--}}
            {{--                                                   class="input-text required-entry validate-email"--}}
            {{--                                                   placeholder="Enter your email address">--}}
            {{--                                            <button type="submit" title="Subscribe" class="button subscribe"><span>Subscribe</span>--}}
            {{--                                            </button>--}}
            {{--                                        </div>--}}
            {{--                                        <!--container_form_news2-->--}}
            {{--                                    </div>--}}
            {{--                                    <!--container_form_news-->--}}
            {{--                                </form>--}}
            {{--                            </div>--}}
            {{--                            <!--newsletter-wrap-->--}}
            {{--                        </div>--}}
            {{--                    </div>--}}
            {{--                </div>--}}
            {{--                <!--footer-column-last-->--}}
            {{--            </div>--}}
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-xs-12 col-lg-4">
                        <div class="co-info">
                            <h4>ADDRESS</h4>
                            <address>
                                <div><span>No 89,
 	Abbey Park Road,
 	Leicester,
 	<br>LE4 5AP
 	UK</span>

    </div>
                            </address>
                            <a href="https://fb.me/AS.ImportsAndExports" class="fa fa-facebook" target="_blank" style="margin-left: 10px;"></a>
 <a href="https://www.instagram.com/as_exports/" class="fa fa-instagram" target="_blank" style="margin-left: 20px;"></a>
                        </div>
                    </div>
                    <div class="col-sm-8 col-xs-12 col-lg-8">
                        <div class="footer-column">
                            <h4>CALL NOW</h4>
                            <div><a href="tel:00441162662153"> 0044 116 266 2153</a></div>
                            <div><a href="tel:00447859178849"> 0044 785 917 8849</a></div>
                        </div>
                        <div class="footer-column">
                            <h4>MAIL NOW</h4>
                            <div><span><a href="#">	info@as-exports.co.uk</a></span></div>
                        </div>
                        <div class="footer-column">
                            <h4>OPEN HOURS</h4>
                            <div><span>Mon - Fri : 09am to 06pm</span></div>
                        </div>
                    </div>

                    <!--col-sm-12 col-xs-12 col-lg-8-->
                    <!--col-xs-12 col-lg-4-->
                </div>
                <!--row-->

            </div>

            <!--container-->
        </div>

        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-4">
                        {{--                        <div class="social">--}}
                        {{--                            <ul>--}}
                        {{--                                <li class="fb"><a href="#"></a></li>--}}
                        {{--                                <li class="tw"><a href="#"></a></li>--}}
                        {{--                                <li class="googleplus"><a href="#"></a></li>--}}
                        {{--                                <li class="rss"><a href="#"></a></li>--}}
                        {{--                                <li class="pintrest"><a href="#"></a></li>--}}
                        {{--                                <li class="linkedin"><a href="#"></a></li>--}}
                        {{--                                <li class="youtube"><a href="#"></a></li>--}}
                        {{--                            </ul>--}}
                        {{--                        </div>--}}
                    </div>
                    <div class="col-sm-4 col-xs-12 coppyright"><p style="color: white">&copy; {{date("Y")}} A & S
                            Imports and Exports Ltd. All Rights Reserved. </p>Powered By Cyntrek Solutions
                    </div>
                    {{--                    <div class="col-xs-12 col-sm-4">--}}
                    {{--                        <div class="payment-accept"> <img src="images/payment-1.png" alt=""> <img src="images/payment-2.png" alt=""> <img src="images/payment-3.png" alt=""> <img src="images/payment-4.png" alt=""> </div>--}}
                    {{--                    </div>--}}
                </div>
            </div>
        </div>
        <!-- BEGIN SIMPLE FOOTER -->
    </footer>
    <!-- End For version 1,2,3,4,6 -->

</div>
<!--page-->
<!-- Mobile Menu-->
<div id="mobile-menu">
    <ul>
        <li class="active"><a class="level-top" href="#"><span>Home</span></a></li>
        <li class="active"><a class="level-top" href="/listing"><span>Our stock</span></a></li>
        <li class="active"><a class="level-top" href="{{route('order-now')}}"><span>Order Now</span></a></li>
        <li class="active"><a class="level-top" href="{{route('aboutus')}}"><span>About Us</span></a></li>
        <li class="active"><a class="level-top" href="{{route('contactus')}}"><span>Contact Us</span></a></li>
    </ul>
</div>
{{--<div class="popup1" style="display: block;">--}}
{{--    <div class="newsletter-sign-box">--}}
{{--        <h3>Newsletter Sign up</h3>--}}
{{--        <img src="images/close-icon.png" alt="close" class="x" onClick="HideMe();">--}}
{{--        <div class="newsletter">--}}
{{--            <div class="newsletter_img"><img alt="newsletter" src="images/newsletter_img.png"></div>--}}
{{--            <form method="post" id="popup-newsletter" name="popup-newsletter" class="email-form">--}}
{{--                <h4>sign up for our exclusive email list and be the first to hear of special offers.</h4>--}}
{{--                <div class="newsletter-form">--}}
{{--                    <div class="input-box">--}}
{{--                        <input type="text" name="email" id="newsletter2" title="Sign up for our newsletter"--}}
{{--                               placeholder="Enter your email address" class="input-text required-entry validate-email">--}}
{{--                        <button type="submit" title="Subscribe" class="button subscribe"><span>Subscribe</span></button>--}}
{{--                    </div>--}}
{{--                    <!--input-box-->--}}
{{--                </div>--}}
{{--                <!--newsletter-form-->--}}
{{--                <label class="subscribe-bottom">--}}
{{--                    <input type="checkbox" name="notshowpopup" id="notshowpopup">--}}
{{--                    Don’t show this popup again</label>--}}
{{--            </form>--}}
{{--        </div>--}}
{{--        <!--newsletter-->--}}

{{--    </div>--}}
{{--    <!--newsletter-sign-box-->--}}
{{--</div>--}}
{{--<div id="fade" style="display: block;"></div>--}}

<!-- JavaScript -->
<script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-slider.min.js')}}"></script>
<script src="{{asset('js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/parallax.js')}}"></script>
<script type="text/javascript" src="{{asset('js/revslider.js')}}"></script>
<script type="text/javascript" src="{{asset('js/common.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.bxslider.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jquery.mobile-menu.min.js')}}"></script>
<script src="{{asset('js/countdown.js')}}"></script>
<script>
    jQuery(document).ready(function () {
        jQuery('#rev_slider_4').show().revolution({
            dottedOverlay: 'none',
            delay: 5000,
            startwidth: 1170,
            startheight: 650,

            hideThumbs: 200,
            thumbWidth: 200,
            thumbHeight: 50,
            thumbAmount: 2,

            navigationType: 'thumb',
            navigationArrows: 'solo',
            navigationStyle: 'round',

            touchenabled: 'on',
            onHoverStop: 'on',

            swipe_velocity: 0.7,
            swipe_min_touches: 1,
            swipe_max_touches: 1,
            drag_block_vertical: false,

            spinner: 'spinner0',
            keyboardNavigation: 'off',

            navigationHAlign: 'center',
            navigationVAlign: 'bottom',
            navigationHOffset: 0,
            navigationVOffset: 20,

            soloArrowLeftHalign: 'left',
            soloArrowLeftValign: 'center',
            soloArrowLeftHOffset: 20,
            soloArrowLeftVOffset: 0,

            soloArrowRightHalign: 'right',
            soloArrowRightValign: 'center',
            soloArrowRightHOffset: 20,
            soloArrowRightVOffset: 0,

            shadow: 0,
            fullWidth: 'on',
            fullScreen: 'off',

            stopLoop: 'off',
            stopAfterLoops: -1,
            stopAtSlide: -1,

            shuffle: 'off',

            autoHeight: 'off',
            forceFullWidth: 'on',
            fullScreenAlignForce: 'off',
            minFullScreenHeight: 0,
            hideNavDelayOnMobile: 1500,

            hideThumbsOnMobile: 'off',
            hideBulletsOnMobile: 'off',
            hideArrowsOnMobile: 'off',
            hideThumbsUnderResolution: 0,

            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            startWithSlide: 0,
            fullScreenOffsetContainer: ''
        });
    });
</script>
<script type="text/javascript">
    function HideMe() {
        jQuery('.popup1').hide();
        jQuery('#fade').hide();
    }
</script>
<script>
    var dthen1 = new Date("03/31/20 11:59:00 PM");
    start = "03/01/20 03:02:11 AM";
    start_date = Date.parse(start);
    var dnow1 = new Date(start_date);
    if (CountStepper > 0)
        ddiff = new Date((dnow1) - (dthen1));
    else
        ddiff = new Date((dthen1) - (dnow1));
    gsecs1 = Math.floor(ddiff.valueOf() / 1000);

    var iid1 = "countbox_1";
    CountBack_slider(gsecs1, "countbox_1", 1);
</script>
<!-- <script>
    jQuery('#brand_drop').on('change', function () {
        let selected = this.value;
        window.location.href = "/?brand="+selected;
    });

    jQuery('#brand_dropx').on('change', function () {
        let selected = this.value;
        window.location.href = "/listing?brand="+selected;
    });
</script> -->
</body>
</html>
