@extends('layouts.master')

@section('content')
    <div class="page-heading" style="background: url('../images/slider/mercs.jpg');background-position: center;background-size: cover;background-blend-mode: color;background-color: rgb(0,0,0,0.3);">
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                    </div>
                </div>
            </div>
        </div>

        <div class="page-title">
            <h2>PRODUCT LISTING</h2>
        </div>
    </div>

    <section class="main-container col2-left-layout bounceInUp animated">

        <div class="container">
            <div class="row">
                <div class="col-main col-sm-9 col-sm-push-3 product-grid">
                    <div class="pro-coloumn">
                        <article class="col-main">
                            <div class="category-products">
                                <ol class="products-list" id="products-list">
                                    @foreach($vehicles as $vehicle)
                                        <li class="item even">
                                            <div class="product-image">
                                                <a href="{{route('single.car',$vehicle->id)}}" title="HTC Rhyme Sense">
                                                    <img class="small-image"
                                                         src="{{asset('uploads/vehicles/'.$vehicle->images[0]->file_name)}}"
                                                         alt="HTC Rhyme Sense">
                                                </a>
                                            </div>

                                            <div class="product-shop">
                                                <h2 class="product-name">
                                                    <a href="{{route('single.car',$vehicle->id)}}"
                                                       title="HTC Rhyme Sense">{{$vehicle->title}}</a>
                                                </h2>

                                                <div class="desc std">
                                                    {{$vehicle->overview}}
                                                </div>

                                                <div class="price-box">
                                                    <p class="special-price">
                                                        <span class="price-label"></span>
                                                        @if ($vehicle->price)
                                                            <span id="product-price-212"
                                                                  class="price">{{'$ '.$vehicle->price}}</span>
                                                        @else
                                                            <span id="product-price-212"
                                                                  class="price">{{($vehicle->start_price)?'$ '.$vehicle->start_price.' - '.'$ '.$vehicle->end_price:'Negotiable'}}</span>
                                                        @endif

                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ol>
                            </div>
                            <div class="toolbar bottom">
                                <div id="sort-by">
                                    <a class="button-asc left" href="#" title="Set Descending Direction"><span
                                            class="top_arrow">
                                        </span>
                                    </a>
                                </div>

                                <div class="pager">
                                    <div id="limiter">

                                    </div>
                                    <div class="pages">
                                        {{--                                        <label>Page:</label>--}}
                                        {{--                                        <ul class="pagination">--}}

                                        {{--                                            <li><a href="#">&laquo;</a></li>--}}
                                        {{--                                            <li class="active"><a href="#">1</a></li>--}}
                                        {{--                                            <li><a href="#">2</a></li>--}}
                                        {{--                                            <li><a href="#">3</a></li>--}}
                                        {{--                                            <li><a href="#">4</a></li>--}}
                                        {{--                                            <li><a href="#">5</a></li>--}}
                                        {{--                                            <li><a href="#">&raquo;</a></li>--}}
                                        {{--                                        </ul>--}}
                                    </div>
                                </div>
                            </div>

                        </article>
                    </div>
                </div>


                <aside class="col-left sidebar col-sm-3 col-xs-12 col-sm-pull-9 wow bounceInUp animated">
                    <div class="section-filter">
                        <div class="b-filter__inner bg-grey">
                            <h2>Find your right car</h2>
                            {!!  Form::open(['route' => 'listing.index','class'=>'b-filter','method'=>'get']) !!}
                            <div class="btn-group bootstrap-select" style="width: 100%;">
                                {!! Form::select('brand',$brands->pluck('name','id'),null,['class'=>'selectpicker','data-width'=>"100%",'placeholder'=>'Select Brand','id'=>'brand_dropx']) !!}
                            </div>
                           <!--  <div class="btn-group bootstrap-select" style="width: 100%;">
                                {!! Form::select('model',$models->pluck('name','id'),null,['class'=>'selectpicker','data-width'=>"100%",'placeholder'=>'Select Model']) !!}
                            </div> -->

{{--                            <div class="btn-group bootstrap-select" style="width: 100%;">--}}
{{--                                {!! Form::select('year',\App\Vehicle::years,null,['class'=>'selectpicker','data-width'=>"100%",'placeholder'=>'Select Year']) !!}--}}
{{--                            </div>--}}

                            <div>
                                <div class="b-filter__btns">
                                    <button class="btn btn-lg btn-primary">search vehicle</button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                    <div class="custom-slider">
                        <div>
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li class="active" data-target="#carousel-example-generic" data-slide-to="0"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="item active"><img src="images/slide3.jpg" alt="slide3">
                                        <div class="carousel-caption">
                                            <h3><a title=" Sample Product" href="product-detail.html">Trustability and Quality is a Gift from us to you</a></h3>
                                            <p></p>
                                            <a class="link" href="#">Buy Now</a></div>
                                    </div>
                                    <div class="item"><img src="images/slide1.jpg" alt="slide1">
                                        <div class="carousel-caption">
                                            <h3><a title=" Sample Product" href="product-detail.html">Exporting vehicles right to your port</a>
                                            </h3>
                                           <!--  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
                                        </div>
                                    </div>
                                    <div class="item"><img src="images/slide2.jpg" alt="slide2">
                                        <div class="carousel-caption">
                                            <h3><a title=" Sample Product" href="product-detail.html">Order brand new car of your choice in bulk</a></h3>
                                            <!-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p> -->
                                        </div>
                                    </div>
                                </div>
                                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                    <span class="sr-only">Previous</span> </a> <a class="right carousel-control"
                                                                                  href="#carousel-example-generic"
                                                                                  data-slide="next"> <span
                                        class="sr-only">Next</span> </a></div>
                        </div>
                    </div>

                </aside>

            </div>
            <!--row-->
        </div>

    </section>

@endsection
